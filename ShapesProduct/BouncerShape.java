/**
 * Forme rebondissante
 *
 * @author Mathieu Monteverde et Michela Zucca
 *
 * Date: 23.03.2017 
 * 
 * File: BouncerShape.java
 */
package ShapesProduct;

import GUI.Display;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.util.Random;

public abstract class BouncerShape implements Bouncable {

   // Valeurs statiques utiles pour la création des formes de type BouncerShape
   protected static final int MAX_DELTA_XY = 7;
   protected static final int MIN_SIZE = 8;
   protected static final int MAX_SIZE = 20;

   // Displayer dans lequel les formes doivent se dessiner
   private final Display display;

   // Position et vitesse de déplacement de la forme
   private int x;
   private int y;
   private int deltaX;
   private int deltaY;

   /**
    * Constructeur d'un BouncerShape. Le vecteur de déplacement (deltaX,DeltaY)
    * est généré aléatoirement. Les directions (0,y), (x,0) ou (0,0) 
    * sont exclues. La position de départ correspond au centre de la zone de 
    * dessin
    */
   public BouncerShape() {
      Random random = new Random();
      this.display = Display.getInstance();
      // Position de départ centrée
      this.x = display.getWidth() / 2;
      this.y = display.getHeight() / 2;
     
      // Calcul des directions 
      this.deltaX = random.nextInt(MAX_DELTA_XY) - random.nextInt(MAX_DELTA_XY);
      this.deltaY = random.nextInt(MAX_DELTA_XY) - random.nextInt(MAX_DELTA_XY);
      
// Elimination du cas 0 pour un rendu plus agréable
      if (deltaX == 0) {
         deltaX += random.nextBoolean() ? 1 : -1;
      }
      if (deltaY == 0) {
         deltaY += random.nextBoolean() ? 1 : -1;
      }
   }

   /**
    * Position sur l'axe des abscisses, depuis le coin surpérieur gauche
    *
    * @return la position en x de la forme
    */
   public int getX() {
      return x;
   }

   /**
    * Position sur l'axe des ordonnées, depuis le coin supérieur gauche
    *
    * @return la position y de la forme
    */
   public int getY() {
      return y;
   }

   /**
    * Deplacement sur l'axe des abscisses
    *
    * @return deplacement en x de la forme
    */
   public int getDeltaX() {
      return deltaX;
   }

   /**
    * Deplacement sur l'axe des ordonnées
    *
    * @return deplacement en y de la forme
    */
   public int getDeltaY() {
      return deltaY;
   }

   /**
    * Déplace la forme en fonction de sa vitesse actuelle. Cette méthode assure
    * également que l'objet ne sorte pas des bornes du Display display, en
    * empêchant les formes d'avoir une position (respectivement y) négative ou
    * plus grande que la largeur (respectivement la hauteur) du display.
    */
   @Override
   public void move() {
      // Déplacer la forme
      x += deltaX;
      y += deltaY;

      /*
       * En cas de dépassement des bornes, replacer la forme et inverser sa 
       * vitesse.
       */
      if (x + getWidth() > display.getWidth()) { // largeur max
         x = display.getWidth() - getWidth();
         deltaX *= -1;
      } else if (x < 0) {  // largeur min
         x = 0;
         deltaX *= -1;
      }
      if (y + getHeight() > display.getHeight()) { // hauteur max
         y = display.getHeight() - getHeight();
         deltaY *= -1;
      } else if (y < 0) { // hauter min
         y = 0;
         deltaY *= -1;
      }

      // Mettre à jour la position de la forme Shape correspondante.
      updateShapePosition();
   }

   /**
    * Retourne le Graphics2D du Display sur lequel se desisne la forme.
    *
    * @return Graphics2D Graphcis2D où les formes se dessinent
    */
   public Graphics2D getGraphics() {
      return display.getGraphics();
   }

   /**
    * La méthode draw se charge de dessiner la forme sur le Display.
    */
   @Override
   public void draw() {
      getRenderer().display(getGraphics(), this);
   }

   /**
    * Retourne la largeur du plus petit rectangle contenant la forme. Chaque
    * classe enfant doit calculer cette largeur en fonction de la forme qu'elle
    * représente.
    *
    * @return la largeur du plus petit rectangle contenant la forme
    */
   public abstract int getWidth();

   /**
    * Retourne la hauteur du plus petit rectangle contenant la forme. Chaque
    * classe enfant doit calculer cette hauteur en fonction de la forme qu'elle
    * représente.
    *
    * @return la hauteur du plus petit rectangle contenant la forme
    */
   public abstract int getHeight();

   /**
    * La méthode abstraite updateShapePosition doit être implémentée par les
    * enfants et mettre à jour la position de la forme qu'elles représentent.
    * Cette méthode est appelée par défaut après chaque déplacement effectué
    * avec la méthode move().
    */
   public abstract void updateShapePosition();

   /**
    * Retourne la Shape que représenter la forme.
    *
    * @return Shape forme représentée par la classe enfant
    */
   @Override
   public abstract Shape getShape();
}
