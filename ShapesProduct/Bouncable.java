/**
 * Interface d'une forme rebondissante
 *
 * @author Mathieu Monteverde et Michela Zucca
 *
 * Date: 30.03.2017
 *
 * File: Bouncable.java
 */
package ShapesProduct;

import Renderable.Renderable;
import java.awt.Color;
import java.awt.Shape;

public interface Bouncable {

   /**
    * Dessiner la forme
    */
   void draw();

   /**
    * Déplacer la forme
    */
   void move();

   /**
    * Gestionnaire de dessin lié à la forme
    * @return 
    */
   Renderable getRenderer();

   /**
    * Récupérer la couleur de la forme
    * @return couleur
    */
   Color getColor();

   /**
    * Récupérer la forme
    * @return forme
    */
   Shape getShape();
}
