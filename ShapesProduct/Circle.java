/**
 * Forme rebondissante de type cercle
 *
 * @author Mathieu Monteverde et Michela Zucca
 *
 * Date: 23.03.2017
 *
 * File: Circle.java
 */
package ShapesProduct;

import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.util.Random;

public abstract class Circle extends BouncerShape {

   private int rayon;
   private final Ellipse2D ellipse;

   /**
    * Constructeur d'un cercle
    * Le rayon est généré aléatoirement entre MIN_SIZE et MAX_SIZE
    * et est de valeur entière
    */
   public Circle() {
      super();
      Random random = new Random();
      // Création du cercle
      ellipse = new Ellipse2D.Double();
      // Déterminer la taille
      this.rayon = random.nextInt(MAX_SIZE) + MIN_SIZE;
      ellipse.setFrame(getX(), getY(), getWidth(), getHeight());
   }

   /**
    * Retourne la largeur du cercle, soit le diamètre
    *
    * @return largeur
    */
   @Override
   public int getWidth() {
      return 2 * rayon;
   }

   /**
    * Retourne la hauteur du cercle, soit le diamètre
    *
    * @return hauteur
    */
   @Override
   public int getHeight() {
      return 2 * rayon;
   }

   /**
    * Retourne le rayon du cercle
    *
    * @return rayon
    */
   public int getRayon() {
      return rayon;
   }

   /**
    * Met à jour la position de la forme
    */
   @Override
   public void updateShapePosition() {
      Ellipse2D.Double e = (Ellipse2D.Double) getShape();
      e.setFrame(getX(), getY(), getWidth(), getHeight());
   }

   /**
    * Retourne la forme correspondant à un cercle
    *
    * @return forme
    */
   @Override
   public Shape getShape() {
      return ellipse;
   }
}
