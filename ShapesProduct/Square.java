/**
 * Forme rebondissante de type carré
 *
 * @author Mathieu Monteverde et Michela Zucca
 *
 * Date: 24.03.2017 
 * 
 * File: Square.java
 */
package ShapesProduct;

import java.awt.Rectangle;
import java.awt.Shape;
import java.util.Random;

public abstract class Square extends BouncerShape {

   private int edge;
   private final Rectangle rectangle;

   /**
    * Constructeur d'un carré
    * la largeur des côtés est générée aléatoirement entre MIN_SIZE et MAX_SIZE
    */
   public Square() {
      super();
      Random random = new Random();
      // Création de la forme
      rectangle = new Rectangle();
      // Déterminer la taille
      this.edge = random.nextInt(MAX_SIZE) + MIN_SIZE;
      rectangle.setFrame(getX(), getY(), getWidth(), getHeight());
   }

   /**
    * Taille en largeur du carré
    *
    * @return largeur
    */
   @Override
   public int getWidth() {
      return edge;
   }

   /**
    * Taille en hauteur du carré
    *
    * @return hauteur
    */
   @Override
   public int getHeight() {
      return edge;
   }

   /**
    * Taille du bord du carré
    *
    * @return bord
    */
   public int getEdge() {
      return edge;
   }

   /**
    * Mise à jour de la position du carré
    */
   @Override
   public void updateShapePosition() {
      Rectangle e = (Rectangle) getShape();
      e.setFrame(getX(), getY(), getWidth(), getHeight());
   }

   /**
    * Retourne la forme correspondant à un carré
    *
    * @return forme
    */
   @Override
   public Shape getShape() {
      return rectangle;
   }
}
