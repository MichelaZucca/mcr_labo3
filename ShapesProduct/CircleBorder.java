/**
 * Cercle à bordure
 *
 * @author Mathieu Monteverde et Michela Zucca
 *
 * Date: 30.03.2017
 *
 * File: CircleBorder.java
 */
package ShapesProduct;

import Renderable.Renderable;
import Renderable.RenderableBorder;
import java.awt.Color;

public class CircleBorder extends Circle {

   /**
    * Constructeur
    */
   public CircleBorder() {
      super();
   }

   /**
    * Retourne la couleur de la forme
    *
    * @return couleur
    */
   @Override
   public Color getColor() {
      return Color.GREEN;
   }

   /**
    * Retourne le Renderer correspondant à une forme bordure
    *
    * @return Renderer
    */
   @Override
   public Renderable getRenderer() {
      return RenderableBorder.getInstance();
   }
}
