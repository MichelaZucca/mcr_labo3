/**
 * Cercle plein
 *
 * @author Mathieu Monteverde et Michela Zucca
 *
 * Date: 30.03.2017 
 * 
 * File: CircleFilled.java
 */
package ShapesProduct;

import Renderable.Renderable;
import Renderable.RenderableFill;
import java.awt.Color;

public class CircleFilled extends Circle {

   /**
    * Constructeur
    */
   public CircleFilled() {
      super();
   }

   /**
    * Retourne la couleur de la forme
    *
    * @return couleur
    */
   @Override
   public Color getColor() {
      return Color.BLUE;
   }

   /**
    * Retourne le Renderer correspondant à une forme pleine
    *
    * @return Renderer
    */
   @Override
   public Renderable getRenderer() {
      return RenderableFill.getInstance();
   }
}
