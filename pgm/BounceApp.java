/**
 * Application des bouncers.
 * Gestion au clavier par l'utilisateur
 * - touche f : création de cercles et carrés pleins. 
 * - touche b : création de cercles et carrés à bordures
 * - touche e : effac l'affichage
 * - touche q : quitte le programme
 *
 * @author Mathieu Monteverde et Michela Zucca
 *
 * Date: 30.03.2017
 *
 * File: BounceApp.java
 */
package pgm;

import GUI.Display;
import ShapeFactory.BorderShapesFactory;
import ShapeFactory.FilledShapesFactory;
import ShapeFactory.ShapesFactory;
import java.util.LinkedList;
import ShapesProduct.BouncerShape;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import static java.lang.Thread.sleep;

public class BounceApp {

   // Liste des formes actuellement présentent dans l'affichage
   private LinkedList<BouncerShape> shapes; 
   // Affichage
   private Display display;
   // Fabrique 
   private ShapesFactory factory;

   /**
    * Constructeur pour créer une nouvelle BounceApp.
    */
   public BounceApp() {
      display = Display.getInstance();
      shapes = new LinkedList<>();
      factory = new BorderShapesFactory();
   }

   /**
    * Méthode à appeler pour démarrer une application BouceApp.
    */
   public void loop() {
      // Associer le KeyAdapter à l'affichage utilisé
      display.addKeyAdapter(new KeyAdapter() {

         @Override
         public void keyTyped(KeyEvent e) {
            // Examiner l'evenement 
            switch (e.getKeyChar()) {
               // Effacer l'affichage
               case 'e':
                   // Synchronisation de l'accès sur la liste des formes
                  synchronized (shapes) {
                     shapes.clear();
                     break;
                  }
               // Générer 10 cercles et 10 carrés possédant une bordure
               case 'b':
                   // Synchronisation de l'accès sur la liste des formes
                  synchronized (shapes) {
                     // Mise à jour de la fabrique utilisée
                     factory = new BorderShapesFactory();
                     for (int i = 0; i < 10; i++) {
                        shapes.add(factory.createCircle());
                        shapes.add(factory.createSquare());
                     }
                  }
                  break;
               // Générer 10 cercles et 10 carrés pleins
               case 'f':
                   // Synchronisation de l'accès sur la liste des formes
                  synchronized (shapes) {
                     // Mise à jour de la fabrique utilisée
                     factory = new FilledShapesFactory();
                     for (int i = 0; i < 10; i++) {
                        shapes.add(factory.createCircle());
                        shapes.add(factory.createSquare());
                     }
                  }
                  break;
               // Quitter le programme
               case 'q':
                  System.exit(0);
                  break;
            }
         }
      });

      Thread thread = new Thread() {
         @Override
         public void run() {
            while (true) {
               try {
                  sleep(33); // Temporisation
               } catch (Exception e) {
                  e.getMessage();
               }
               // Redessiner le rectangle de fond
               display.clrImage(); 
               // Synchronisation de l'accès sur la liste des formes
               synchronized (shapes) {
                  for (BouncerShape o : shapes) {
                     o.move();
                     o.draw();
                  }
               }
               display.repaint();
            }
         }
      };
      //Démarrer le thread
      thread.start();
   }

   public static void main(String[] args) {
      new BounceApp().loop();
   }
}
