/**
 * Dessinateur des formes pleines Boucable sur un Graphics2D
 *
 * @author Mathieu Monteverde et Michela Zucca
 *
 * Date: 30.03.2017
 *
 * File: RenderableFill.java
 */
package Renderable;

import ShapesProduct.Bouncable;
import java.awt.Graphics2D;

public class RenderableFill implements Renderable {

   // singleton
   private static RenderableFill instance;
   
   /**
    * Constructeur privé
    */
   private RenderableFill(){
   }

   /**
    * Retourne le singleton
    *
    * @return singleton
    */
   public static RenderableFill getInstance() {
      if (instance == null) {
         instance = new RenderableFill();
      }
      return instance;
   }

   /**
    * Dessine une forme Boucable pleine dans la zone de dessin Graphics2D
    *
    * @param g Graphics2D, zone de dessin
    * @param b forme à dessiner
    */
   @Override
   public void display(Graphics2D g, Bouncable b) {
      g.setColor(b.getColor());
      g.fill(b.getShape());
   }
}
