/**
 * Dessinateur des formes à bordure Boucable sur un Graphics2D
 *
 * @author Mathieu Monteverde et Michela Zucca
 *
 * Date: 30.03.2017
 *
 * File: RenderableBorder.java
 */
package Renderable;

import ShapesProduct.Bouncable;
import java.awt.BasicStroke;
import java.awt.Graphics2D;

public class RenderableBorder implements Renderable {

   // singleton
   private static RenderableBorder instance;
   
   /** 
    * Constructeur privé
    */
   private RenderableBorder(){
   }

   /**
    * Retourne le singleton
    *
    * @return singleton
    */
   public static Renderable getInstance() {
      if (instance == null) {
         instance = new RenderableBorder();
      }
      return instance;
   }

   /**
    * Dessine une forme Boucable à bordure dans la zone de dessin Graphics2D
    *
    * @param g Graphics2D, zone de dessin
    * @param b forme à dessiner
    */
   @Override
   public void display(Graphics2D g, Bouncable b) {
      g.setColor(b.getColor());
      g.setStroke(new BasicStroke(2));
      g.draw(b.getShape());
   }
}
