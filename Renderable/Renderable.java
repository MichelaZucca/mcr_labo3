/**
 * Interface qui permet de dessiner des formes Boucable sur un Graphics2D
 *
 * @author Mathieu Monteverde et Michela Zucca
 *
 * Date: 30.03.2017 File: 
 * 
 * Renderable.java
 */
package Renderable;

import ShapesProduct.Bouncable;
import java.awt.Graphics2D;

public interface Renderable {
   /**
    * Dessine la forme Boucable sur un Graphics2D
    * @param g zone de dessin Graphics2D
    * @param b forme à dessiner
    */
   void display(Graphics2D g, Bouncable b);

}
