/**
 * Affichage utilisé par les Boucable
 *
 * @author Mathieu Monteverde et Michela Zucca
 *
 * Date: 23.03.2017
 *
 * File: Display.java
 */
package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Display extends JFrame implements Displayer {

   // Singleton
   private static Display instance;
   // Composants d'affichage
   private final JPanel panel;
   private BufferedImage image;

   /**
    * Constructeur privé
    */
   private Display() {
      // Création et paramètrage de la fenêtre
      super.setTitle("Bouncers");
      setVisible(true);
      setLocation(200, 200);
      setDefaultCloseOperation(EXIT_ON_CLOSE);

      panel = new JPanel();
      panel.setBackground(Color.WHITE);
      panel.setPreferredSize(new Dimension(500, 400));
      this.add(panel);

      image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
      pack();
   }

   /**
    * Retourne le singleton
    *
    * @return singleton
    */
   public static Display getInstance() {
      if (instance == null) {
         instance = new Display();
      }
      return instance;
   }

   /**
    * Largeur de la fenêtre
    *
    * @return largeur de la fenêtre
    */
   @Override
   public int getWidth() {
      return super.getWidth();
   }

   /**
    * Hauteur de la fenêtre
    *
    * @return hauteur de la fenêtre
    */
   @Override
   public int getHeight() {
      return super.getHeight();
   }

   /**
    * Zone de dessin de la fenêtre
    *
    * @return Graphics2D où dessiner
    */
   @Override
   public Graphics2D getGraphics() {
      return (Graphics2D) image.getGraphics();
   }

   /**
    * Repaindre l'image
    */
   @Override
   public void repaint() {
      super.getGraphics().drawImage(image, 0, 0, null);
   }

   /**
    * Modifier le titre de la fenêtre
    *
    * @param s nouveau titre
    */
   @Override
   public void setTitle(String s) {
      super.setTitle(s);
   }

   /**
    * Raffraihir l'image de fond
    */
   public void clrImage() {
      image = new BufferedImage(getWidth(), getHeight(), image.getType());
      image.getGraphics().setColor(Color.white);
      image.getGraphics().fillRect(0, 0, getWidth(), getHeight());
   }

   /**
    * Ajouter un KeyAdapter à la fenêtre
    *
    * @param key KeyAdapter a lié
    */
   public void addKeyAdapter(KeyAdapter key) {
      super.addKeyListener(key);
   }
}
