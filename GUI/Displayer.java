/**
 * Interface utiliser par l'affichage
 *
 * @author Mathieu Monteverde et Michela Zucca
 *
 * Date: 23.03.2017 
 * 
 * File: Displayer.java
 */
package GUI;

import java.awt.Graphics2D;

public interface Displayer {

   int getWidth();

   int getHeight();

   Graphics2D getGraphics();

   void repaint();

   void setTitle(String s);
}
