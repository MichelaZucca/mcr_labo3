/**
 * Fabrique des formes pleines
 *
 * @author Mathieu Monteverde et Michela Zucca
 *
 * Date: 30.03.2017
 *
 * File: FilledShapesFactory.java
 */
package ShapeFactory;

import ShapesProduct.Circle;
import ShapesProduct.CircleFilled;
import ShapesProduct.Square;
import ShapesProduct.SquareFilled;

public class FilledShapesFactory extends ShapesFactory {

   /**
    * Créer un carré plein
    *
    * @return carré plein
    */
   @Override
   public Square createSquare() {
      return new SquareFilled();
   }

   /**
    * Créer un cercle plein
    *
    * @return cercle plein
    */
   @Override
   public Circle createCircle() {
      return new CircleFilled();
   }
}
