/**
 * Fabrique des formes à bordure
 *
 * @author Mathieu Monteverde et Michela Zucca
 *
 * Date: 30.03.2017
 *
 * File: BorderShapesFactory.java
 */
package ShapeFactory;

import ShapesProduct.Circle;
import ShapesProduct.CircleBorder;
import ShapesProduct.Square;
import ShapesProduct.SquareBorder;

public class BorderShapesFactory extends ShapesFactory {

   /**
    * Créer un carré à bordure
    *
    * @return carré à bordure
    */
   @Override
   public Square createSquare() {
      return new SquareBorder();
   }

   /**
    * Créer un cercle à bordure
    *
    * @return cercle à bordure
    */
   @Override
   public Circle createCircle() {
      return new CircleBorder();
   }
}
