/**
 * Fabrique abstraite des formes
 *
 * @author Mathieu Monteverde et Michela Zucca
 *
 * Date: 30.03.2017
 *
 * File: ShapesFactory.java
 */
package ShapeFactory;

import ShapesProduct.Circle;
import ShapesProduct.Square;

public abstract class ShapesFactory {

   /**
    * Construction d'un carré
    * @return carré
    */
   public abstract Square createSquare();

   /**
    * Construction d'un cercle
    * @return cercle
    */
   public abstract Circle createCircle();
}
